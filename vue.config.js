module.exports = {
    configureWebpack: {
        module: {
            rules: [
                {
                    test: /\.txt$/,
                    use: [
                        {
                            loader: 'raw-loader'
                        }
                    ]
                }
            ]
        }
    }
};