let DEFAULT_LOCALE = 'en';
let CURRENT_LOCALE = window.localStorage.getItem('locale') || DEFAULT_LOCALE;
let TRANSLATIONS = {};
let CALLBACKS = [];

class Translation {

    constructor(key = '', data = {}) {
        this.key = key;
        this.data = data;
    }

    getRawTranslation() {
        let key = this.key;
        if (CURRENT_LOCALE in TRANSLATIONS && key in TRANSLATIONS[CURRENT_LOCALE]) {
            return TRANSLATIONS[CURRENT_LOCALE][key];
        }
        return key;
    }

    getTranslation() {
        let translation = this.getRawTranslation();
        Object.entries(this.data).forEach(([key, value]) => {
            translation = translation.replace(new RegExp('(\\W|^):' + key + '(\\W|$)', 'g'), '$1' + value + '$2');
        });
        return translation.replace(/^\{[\w\-_]+\}$/, '');
    }

    toString() {
        return this.getTranslation();
    }

    [Symbol.toPrimitive]() {
        return this.toString();
    }

}

function loadTextTranslations() {
    let files = require.context(`@/assets/translations/`, false, /\.txt$/);
    files.keys().forEach(file => {
        let {locale} = file.match(/\/(?<locale>[a-z]{2})\.txt$/).groups;
        if (!TRANSLATIONS[locale]) {
            TRANSLATIONS[locale] = {};
        }
        files(file).default.split('\n').forEach(line => {
            let [key, value] = line.split(/\s*=\s*/);
            if (key && value) {
                TRANSLATIONS[locale][key] = value;
            }
        });
    });
}

function loadFileTranslation() {
    let files = require.context(`@/assets/translations/`, true, /\/[a-z]{2}\/.*\.txt$/);
    files.keys().forEach(file => {
        let {locale, key} = file.match(/\/(?<locale>[a-z]{2})\/(?<key>[\w\-_]+)\.txt$/).groups;
        let content = files(file).default;
        if (!TRANSLATIONS[locale]) {
            TRANSLATIONS[locale] = {};
        }
        TRANSLATIONS[locale][`{${key}}`] = content;
    });
}

loadTextTranslations();
loadFileTranslation();

export function __(text, data = {}) {
    return new Translation(text, data);
}

export function setLocale(locale) {
    if (locale !== CURRENT_LOCALE) {
        CURRENT_LOCALE = locale;
        window.localStorage.setItem('locale', locale);
        CALLBACKS.forEach(callback => callback(locale));
    }
}

export function getLocale() {
    return CURRENT_LOCALE;
}

export function getAvailableLocales() {
    return Object.keys(TRANSLATIONS);
}

export function onChangeLocale(callback) {
    CALLBACKS.push(callback);
}

export function VuePlugin(Vue) {
    const instances = [];
    Vue.prototype.__ = __;
    Vue.mixin({
        created() {
            instances.push(this);
        },
        beforeDestroy() {
            let index = instances.indexOf(this);
            if (index >= 0) {
                instances.splice(index, 1);
            }
        }
    });
    onChangeLocale(() => {
        instances.forEach(instance => instance.$forceUpdate());
    });
}