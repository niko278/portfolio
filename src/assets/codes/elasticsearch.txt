{
    "size": 0,
    "query": {
        "bool": {
            "must": [
                {
                    "terms": {
                        "country": [
                            "us"
                        ]
                    }
                },
                {
                    "range": {
                        "population": {
                            "gte": 1000000
                        }
                    }
                }
            ]
        }
    },
    "aggs": {
        "by_first_letter": {
            "terms": {
                "script": "doc['name'].value.substring(0, 1)",
                "size": 100
            }
        }
    }
}