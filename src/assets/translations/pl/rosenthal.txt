Rosenthal jest sklepem z porcelaną online.
Cały serwis oparty jest na systemie PrestaShop.
Byłem odpowiedzialny zarówno za frontend, jak i backend.
Dodatkowo musiałem napisać parę pluginów do PrestaShop od zera, specjalnie na potrzeby tej strony.
Jednym z nich była integracja z zewnętrznym systemem zarządzania magazynem.