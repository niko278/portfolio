Tetrix Tower jest mobilną grą zręcznościową.
Gra polega na układaniu jak najwyższej wieży z klocków znanych z Tetrisa.
Stworzona została w Unity 3D używając języka C#.
Zrobiłem ją razem ze znajomym, on był odpowiedzialny za modele 3D, a ja za programowanie i całą resztę.
Gra była zintegrowana z Google Play Games API, by móc zapisywać i wyświetlać najlepsze wyniki publicznie.
Była ona dostępna w sklepie Google Play, jednak z powodu braku aktywności została zdjęta.