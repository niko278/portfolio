Site Import is a WordPress plugin, allowing to import data from any other website.
The structure is similar to Best Import, but instead of importing from previously prepared files, you can import directly from other services.
The user just has to select the specified elements (like title, content, date, etc.) on the website he / she wants to import from.
The plugin automatically detects posts and imports them to WordPress.
Similar to Best Import, user can customize every part of the import.