Work as full-stack developer
Create e-commerce services and web applications
Develop and maintain projects
Integrate different services
Improve websites performance
Optimize websites for SEO
Lead small team of developers
- Lyfe - catering online
- Rosenthal - online china shop
- NearPlace - store locator widget
- BlendBerg - simple service selling databases
- Datantify - advanced service selling databases
- Trendbar - medium articles aggregator
- BaseJar - task management application
- Few other smaller projects