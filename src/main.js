import Vue from 'vue';
import App from './components/App.vue';
import {VuePlugin} from './scripts/translations';

Vue.config.productionTip = false;
Vue.use(VuePlugin);

new Vue({
    render: h => h(App)
}).$mount('#app');