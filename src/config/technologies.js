export default [
    {
        name: 'HTML 5',
        image: require('@/assets/images/technologies/html.png'),
        code: require('@/assets/codes/html.txt').default,
        language: 'html'
    }, {
        name: 'CSS 3',
        image: require('@/assets/images/technologies/css.png'),
        code: require('@/assets/codes/css.txt').default,
        language: 'css'
    }, {
        name: 'JavaScript 6',
        image: require('@/assets/images/technologies/js.png'),
        code: require('@/assets/codes/js.txt').default,
        language: 'js'
    }, {
        name: 'PHP 7',
        image: require('@/assets/images/technologies/php.png'),
        code: require('@/assets/codes/php.txt').default,
        language: 'php'
    }, {
        name: 'PHPUnit',
        image: require('@/assets/images/technologies/phpunit.png'),
        code: require('@/assets/codes/phpunit.txt').default,
        language: 'php'
    }, {
        name: 'Laravel',
        image: require('@/assets/images/technologies/laravel.png'),
        code: require('@/assets/codes/laravel.txt').default,
        language: 'php'
    }, {
        name: 'Composer',
        image: require('@/assets/images/technologies/composer.png'),
        code: require('@/assets/codes/composer.txt').default,
        language: 'json'
    }, {
        name: 'ES6',
        image: require('@/assets/images/technologies/es6.png'),
        code: require('@/assets/codes/es6.txt').default,
        language: 'js'
    }, {
        name: 'TypeScript',
        image: require('@/assets/images/technologies/typescript.png'),
        code: require('@/assets/codes/typescript.txt').default,
        language: 'typescript'
    }, {
        name: 'NPM',
        image: require('@/assets/images/technologies/npm.png'),
        code: require('@/assets/codes/npm.txt').default,
        language: 'json'
    }, {
        name: 'Webpack',
        image: require('@/assets/images/technologies/webpack.png'),
        code: require('@/assets/codes/webpack.txt').default,
        language: 'js'
    }, {
        name: 'Vue.js',
        image: require('@/assets/images/technologies/vue.png'),
        code: require('@/assets/codes/vue.txt').default,
        language: 'js'
    }, {
        name: 'React',
        image: require('@/assets/images/technologies/react.png'),
        code: require('@/assets/codes/react.txt').default,
        language: 'js'
    }, {
        name: 'SASS / SCSS',
        image: require('@/assets/images/technologies/sass.png'),
        code: require('@/assets/codes/scss.txt').default,
        language: 'scss'
    }, {
        name: 'Git',
        image: require('@/assets/images/technologies/git.png'),
        code: require('@/assets/codes/git.txt').default,
        language: 'bash'
    }, {
        name: 'REST',
        image: require('@/assets/images/technologies/rest.png'),
        code: require('@/assets/codes/rest.txt').default,
        language: 'txt'
    }, {
        name: 'MySQL',
        image: require('@/assets/images/technologies/mysql.png'),
        code: require('@/assets/codes/mysql.txt').default,
        language: 'sql'
    }, {
        name: 'ElasticSearch',
        image: require('@/assets/images/technologies/elasticsearch.png'),
        code: require('@/assets/codes/elasticsearch.txt').default,
        language: 'json'
    }
];