import {__} from '@/scripts/translations';

export default [
    {
        title: 'Freelancer',
        description: __('{freelancer}'),
        begin: '2013-05',
        end: '2015-06'
    },
    {
        title: 'Zefir Studio',
        description: __('{zefir-studio}'),
        begin: '2015-07',
        end: '2016-03'
    },
    {
        title: 'Normand',
        description: __('{normand}'),
        begin: '2016-04',
        end: '2019-11',
    }
];