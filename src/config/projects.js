import {__} from '@/scripts/translations';

export default [
    {
        name: 'Portfolio',
        description: __('{portfolio}'),
        images: [
            require('@/assets/images/projects/portfolio-about-me.png'),
            require('@/assets/images/projects/portfolio-technologies.png'),
        ]
    }, {
        name: 'Datantify',
        description: __('{datantify}'),
        images: [
            require('@/assets/images/projects/datantify-listing.png'),
            require('@/assets/images/projects/datantify-cart.png')
        ]
    }, {
        name: 'NearPlace',
        description: __('{nearplace}'),
        images: [
            require('@/assets/images/projects/nearplace-widget.png'),
            require('@/assets/images/projects/nearplace-admin.png')
        ]
    }, {
        name: 'TrendBar',
        description: __('{trendbar}'),
        images: [
            require('@/assets/images/projects/trendbar-list.png'),
            require('@/assets/images/projects/trendbar-thumbnails.png')
        ]
    }, {
        name: 'Lyfe',
        description: __('{lyfe}'),
        images: [
            require('@/assets/images/projects/lyfe-home.png'),
            require('@/assets/images/projects/lyfe-admin.png')
        ]
    }, {
        name: 'Rosenthal',
        description: __('{rosenthal}'),
        images: [
            require('@/assets/images/projects/rosenthal-home.png'),
            require('@/assets/images/projects/rosenthal-listing.png')
        ]
    }, {
        name: 'BlendBerg',
        description: __('{blendberg}'),
        images: [
            require('@/assets/images/projects/blendberg-home.png'),
            require('@/assets/images/projects/blendberg-product.png')
        ]
    }, {
        name: 'BaseJar',
        description: __('{basejar}'),
        images: [
            require('@/assets/images/projects/basejar-board.png'),
            require('@/assets/images/projects/basejar-drag.png')
        ]
    }, {
        name: 'Best Import',
        description: __('{best-import}'),
        images: [
            require('@/assets/images/projects/best-import-wordpress.png'),
            require('@/assets/images/projects/best-import-instruction.png')
        ]
    }, {
        name: 'Site Import',
        description: __('{site-import}'),
        images: [
            require('@/assets/images/projects/site-import-landing.png'),
            require('@/assets/images/projects/site-import-import.png')
        ]
    }, {
        name: 'Tetrix Tower',
        description: __('{tetrix-tower}'),
        images: [
            require('@/assets/images/projects/tetrix-tower-jelly.png'),
            require('@/assets/images/projects/tetrix-tower-space.png')
        ]
    }, {
        name: 'Gray Fighter',
        description: __('{gray-fighter}'),
        images: [
            require('@/assets/images/projects/gray-fighter-gameplay.png'),
            require('@/assets/images/projects/gray-fighter-editor.png')
        ]
    }
];