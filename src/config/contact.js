export default {
    fullName: 'Dominik Głodek',
    emailAddress: 'dominik@glodek.me',
    phoneNumber: '+48 514 510 979',
    websiteUrl: 'https://glodek.me/',
    repositoryUrl: 'https://bitbucket.org/niko278/portfolio'
};