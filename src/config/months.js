import {__} from '@/scripts/translations';

export default [
    {
        index: 1,
        name: __('January'),
        season: 'winter'
    },
    {
        index: 2,
        name: __('February'),
        season: 'winter'
    },
    {
        index: 3,
        name: __('March'),
        season: 'spring'
    },
    {
        index: 3,
        name: __('April'),
        season: 'spring'
    },
    {
        index: 5,
        name: __('May'),
        season: 'spring'
    },
    {
        index: 6,
        name: __('June'),
        season: 'summer'
    },
    {
        index: 7,
        name: __('July'),
        season: 'summer'
    },
    {
        index: 8,
        name: __('August'),
        season: 'summer'
    },
    {
        index: 9,
        name: __('September'),
        season: 'autumn'
    },
    {
        index: 10,
        name: __('October'),
        season: 'autumn'
    },
    {
        index: 11,
        name: __('November'),
        season: 'autumn'
    },
    {
        index: 12,
        name: __('December'),
        season: 'winter'
    }
]