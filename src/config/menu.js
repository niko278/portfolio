import {__} from '@/scripts/translations';

export default [
    {
        title: __('Top'),
        icon: require('@/assets/images/icons/top.png'),
        selector: '.header'
    }, {
        title: __('About me'),
        icon: require('@/assets/images/icons/about-me.png'),
        selector: '.about-me'
    }, {
        title: __('Experience'),
        icon: require('@/assets/images/icons/experience.png'),
        selector: '.experience'
    }, {
        title: __('Technologies'),
        icon: require('@/assets/images/icons/technologies.png'),
        selector: '.technologies'
    }, {
        title: __('Projects'),
        icon: require('@/assets/images/icons/projects.png'),
        selector: '.projects'
    }, {
        title: __('Bottom'),
        icon: require('@/assets/images/icons/bottom.png'),
        selector: '.footer'
    }
];